#include <iostream>
#include <fstream>
#include "core.h"

#include "Utils/utils.h"
using namespace std;

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,LPSTR lpCmdLine, int nCmdShow)
#else
    int main(int argc, char *argv[])
#endif
    {
        Core c;

        c.Init(std::string("InitFile.xml"));

        //c.openwindow();

    return 0;
}

#ifdef __cplusplus
}
#endif
