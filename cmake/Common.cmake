macro(SetLibs)

foreach(LIB ${LibList})

    set(LinkLibs ${LinkLibs} ${LibsPath}/${LIB})

endforeach(LIB)

endmacro(SetLibs)


macro(CheckSources)

foreach(Source ${SourceList})

    set(Common_SourceList ${Common_SourceList} src/${Source})

endforeach(Source)

# Need check for existence
set(AvailableSources ${Common_SourceList})

endmacro(CheckSources)



macro(CheckHeaders)

foreach(Header ${HeaderList})

    set(Common_HeaderList ${Common_HeaderList} includes/${Header})

endforeach(Header)

# Need check for existence
set(AvailableHeaders ${Common_HeaderList})

endmacro(CheckHeaders)


macro(SetLibOutputDirectory)

if(StaticLib STREQUAL "")

set(StaticLib OFF)

endif(StaticLib STREQUAL "")

if(UNIX)

    if(NOT StaticLib)

	add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy  ${PROJECT_BINARY_DIR}/lib${PROJECT_NAME}.so  ${BuildPath}/${GameName}/lib${PROJECT_NAME}.so
			     COMMAND ${CMAKE_COMMAND} -E make_directory ${LibsPath}
			     COMMAND ${CMAKE_COMMAND} -E copy  ${PROJECT_BINARY_DIR}/lib${PROJECT_NAME}.so  ${LibsPath}/lib${PROJECT_NAME}.so)
	set_directory_properties(PROPERTIES ADDITIONAL_MAKE_CLEAN_FILES ${LibsPath}/lib${PROJECT_NAME}.so
				 ADDITIONAL_MAKE_CLEAN_FILES ${BuildPath}/${GameName}/lib${PROJECT_NAME}.so)

    else(NOT StaticLib)

	add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD COMMAND ${CMAKE_COMMAND} -E make_directory ${LibsPath}
			     COMMAND ${CMAKE_COMMAND} -E copy  ${PROJECT_BINARY_DIR}/lib${PROJECT_NAME}.a  ${LibsPath}/${PROJECT_NAME}.a)
	set_directory_properties(PROPERTIES ADDITIONAL_MAKE_CLEAN_FILES ${LibsPath}/${PROJECT_NAME}.a)

    endif(NOT StaticLib)

elseif(WIN32)

    if(NOT StaticLib)

	add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy  ${PROJECT_BINARY_DIR}/${PROJECT_NAME}.dll  ${BuildPath}/${GameName}/${PROJECT_NAME}.dll
			COMMAND ${CMAKE_COMMAND} -E make_directory ${LibsPath}
			COMMAND ${CMAKE_COMMAND} -E copy  ${PROJECT_BINARY_DIR}/${PROJECT_NAME}.dll  ${LibsPath}/${PROJECT_NAME}.dll
			COMMAND ${CMAKE_COMMAND} -E copy  ${PROJECT_BINARY_DIR}/${PROJECT_NAME}.lib  ${LibsPath}/${PROJECT_NAME}.lib)
	set_directory_properties(PROPERTIES ADDITIONAL_MAKE_CLEAN_FILES ${LibsPath}/${PROJECT_NAME}.lib
				    ADDITIONAL_MAKE_CLEAN_FILES ${LibsPath}/${PROJECT_NAME}.dll
				    ADDITIONAL_MAKE_CLEAN_FILES ${BuildPath}/${GameName}/${PROJECT_NAME}.dll)

    else(NOT StaticLib)

	add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD COMMAND ${CMAKE_COMMAND} -E make_directory ${LibsPath}
			COMMAND ${CMAKE_COMMAND} -E copy  ${PROJECT_BINARY_DIR}/${PROJECT_NAME}.lib  ${LibsPath}/${PROJECT_NAME}.lib)
	set_directory_properties(PROPERTIES ADDITIONAL_MAKE_CLEAN_FILES ${LibsPath}/${PROJECT_NAME}.lib)

    endif(NOT StaticLib)

endif(UNIX)

endmacro(SetLibOutputDirectory)
