#ifndef CORE_H
#define CORE_H

#include "Utils/utils.h"
#include "Ogre.h"
#include <string>


typedef std::map< std::string,std::string > PathInitializator;
typedef Utils::Container<std::string,std::string> Plugins;
typedef std::map<std::string,std::string>   VideoSettings;

class _PUBLIC_ Core

{

public:

    Core();
    virtual ~Core();

    bool Init(const std::string &strInitFile);

protected:

    bool LoadVideoSettings();
    bool LoadPluginSettings();
    bool LoadPlugins();
    bool InitRenderSystem();
    bool InitVideoSettings();
    bool InitialiseRoot();
    bool InitResources();


private:

    Ogre::Root      *m_Root;
    PathInitializator   m_InitializationPaths;
    VideoSettings       m_VSettings;
    Plugins     m_Plugins;
    Ogre::RenderSystem  *m_Renderer;

};

#endif // CORE_H
