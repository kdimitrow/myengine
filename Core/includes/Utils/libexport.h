#ifndef LIBEXPORT_H
#define LIBEXPORT_H


/* Export Symbol */

#ifdef _WIN32

    #define _EXPORT __declspec(dllexport)

#elif linux

    #if __GNUC__ >= 4

        #define _EXPORT __attribute__ ((visibility ("default")))

    #else

        #define DLL_PUBLIC __attribute__ ((dllexport))

    #endif

#elif mac&&aple

    #define DLL_PUBLIC __attribute__ ((dllexport))

#endif

/* End */

/* Import Symbol */

#ifdef _WIN32

    #define _IMPORT __declspec(dllimport)

#elif linux

    #if __GNUC__ >= 4

        #define _IMPORT __attribute__ ((visibility ("default")))

    #else

        #define _IMPORT __attribute__ ((dllimport))

    #endif

#elif mac&&aple

    #define _IMPORT __attribute__ ((dllimport))

#endif

/* End */


#ifdef UTILITY_LIBRARY

    #define _PUBLIC_UTILITY _EXPORT

#else

    #define _PUBLIC_UTILITY _IMPORT

#endif

#endif // LIBEXPORT_H
