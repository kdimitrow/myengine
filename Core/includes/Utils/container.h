#ifndef CONTAINER_H
#define CONTAINER_H

#include <map>

#include <list>


namespace Utils
{

    template <class _Key,class _Value>
    class Container
    {     

        public:

        typedef std::pair< _Key,_Value > Pair;

        private:

        typedef std::list< Pair > container;

        typedef std::map< _Key,typename container::iterator > finder;

        typedef typename std::map< _Key,typename container::iterator >::iterator fIterator;

        public:

        typedef typename container::iterator iterator;

            /** Default constructor */
            Container()
            {

            }

            /** Copy constructor */
            Container( const Container &C )
            {
                if( this != &C )
                {
                    this->m_Container = C.m_Container;

                    for( iterator it = this->m_Container.begin();it != this->m_Container.end();it++)
                    {
                        this->m_Finder.insert( std::make_pair( it->first,it ) );
                    }
                }
            }

            /** Assignment operator */
            const Container& operator=( const Container &C )
            {
                if( this != &C )
                {
                    this->m_Container.clear();

                    this->m_Finder.clear();

                    this->m_Container = C.m_Container;

                   for( iterator it = this->m_Container.begin();it != this->m_Container.end();it++)
                    {
                        this->m_Finder.insert( std::make_pair( it->first,it ) );
                    }
                }

                return *this;
            }

            /** Equal operator
            *   @return true if both object are the same instance
            */
            bool operator==( const Container &C ) const
            {
                return this == &C;
            }

            /** Not-Equal operator
            *   @return true if both object are not the same instance
            */
            bool operator!=( const Container &C ) const
            {
                return !( *this == C );
            }

            /** Array subscript operator
            *   @return Value for given key
            */
            const _Value operator[]( _Key Key ) const
            {
                return   m_Finder.find( Key )->second->second;
            }

            /** Destructor */
            ~Container()
            {

            }

            /** Insert pair (key,value) */
            void Insert( const _Key &Key,const _Value &Value )
            {
                fIterator it = m_Finder.find( Key );

                if( it == m_Finder.end() )
                {
                    m_Container.push_back( Pair( Key,Value ) );

                    m_Finder.insert( std::make_pair( Key,--( m_Container.end() ) ) );
                }
            }

            /** Erase pair by key */
            void Erase( const _Key &Key )
            {
                fIterator it = m_Finder.find( Key );

                if( it != m_Finder.end() )
                {
                    m_Container.erase( it->second );

                    m_Finder.erase( it );
                }
            }

            /** Erase pair by iterator */
            void Erase( iterator Iterator )
            {
                fIterator it = m_Finder.find( Iterator->first );

                if( it != m_Finder.end() )
                {
                    m_Container.erase( Iterator );

                    m_Finder.erase( it );
                }
            }

            /** Check if is it empty
            *   @return true if empty
            */
            bool Empty() const
            {
                return m_Container.empty();
            }

            /** Clear container */
            void Clear()
            {
                m_Container.clear();

                m_Finder.clear();
            }

            /** Search for element by key
            *   @return Return iterator, If element not found its return iterator to the end
            */
            iterator Find( const _Key &Key )
            {
                fIterator it = m_Finder.find( Key );

                if( it == m_Finder.end() )
                {
                    return this->m_Container.end();
                }

                return it->second;
            }

            /** Return iterator to begining */
            iterator Begin()
            {
                return m_Container.begin();
            }

            /** Return iterator to end */
            iterator End()
            {
                return m_Container.end();
            }

        protected:

            container      m_Container;

            finder     m_Finder;

        private:

    };

}

#endif // CONTAINER_H
