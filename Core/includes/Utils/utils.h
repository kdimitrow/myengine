#ifndef UTILS_H
#define UTILS_H

#include "libexport.h"
#include "defines.h"
#include <vector>
#include <string>
#include "rapidxml.hpp"
#include "tree.h"
#include "container.h"

#ifdef CORE_EXPORT

    #define _PUBLIC_ _EXPORT

#else

    #define _PUBLIC_ _IMPORT

#endif


namespace Utils
{
    _PUBLIC_ void Error( std::string strFile, std::string strFunction, int nLine, std::string strType, std::string strValue);
}

#endif // UTILS_H
