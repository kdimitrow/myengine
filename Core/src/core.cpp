#include "core.h"


Core::Core()
{
    m_Root = 0;
    m_Renderer = 0;
    m_InitializationPaths.insert( std::pair< std::string , std::string >("VSettings","") );
    m_InitializationPaths.insert( std::pair< std::string , std::string >("Plugins","") );
    m_InitializationPaths.insert( std::pair< std::string , std::string >("Resources","") );

}

Core::~Core()
{
    if( m_Root )
        {
            OGRE_DELETE m_Root;

        }
}

bool Core::Init(const std::string &strInitFile)
{
    std::ifstream   InitFile;

    InitFile.open( strInitFile.c_str() ,std::ifstream::in );

    if( !InitFile.is_open() )
    {
        std::string strType("Missing file");
        std::string strValue = strInitFile + std::string( " missing" );
        Utils::Error( __FILE__,__FUNCTION__,__LINE__,strType,strValue );

        return false;
    }

    std::vector<char>   vchTempBuffer( (std::istreambuf_iterator<char>( InitFile )), std::istreambuf_iterator<char>() );

    vchTempBuffer.push_back('\0');
    rapidxml::xml_document<>    PathsFromFile;
    PathsFromFile.parse<0>( &vchTempBuffer[0] );
    rapidxml::xml_node<>    *xmlRoot = PathsFromFile.first_node();
    unsigned int nCheckedPaths = 0;

    for (rapidxml::xml_node<> *child = xmlRoot->first_node(); child; child = child->next_sibling())
    {
        std::string strName;
        std::string strValue;

        strName = child->first_attribute()->value();
        strValue = child->value();

        for( PathInitializator::iterator it = m_InitializationPaths.begin();it != m_InitializationPaths.end() ;it++ )
        {

            if( strName == it->first )
            {
                it->second = strValue;
                nCheckedPaths++;

                break;
            }

        }

        if( nCheckedPaths >= m_InitializationPaths.size() )
        {
            break;
        }

        strName.clear();
        strValue.clear();

    }

    if( !( nCheckedPaths >= m_InitializationPaths.size() ) )
    {
        std::string strType("Incorrect file");
        std::string strValue = strInitFile + std::string( " is incorrect" );

        Utils::Error( __FILE__,__FUNCTION__,__LINE__,strType,strValue );

        return false;
    }

    if( m_InitializationPaths.find( "VSettings" ) != m_InitializationPaths.end() )
    {

        if( !LoadVideoSettings() )

            return false;
    }

    else
    {
        std::string strType("Incorrect file");
        std::string strValue = strInitFile + std::string( " is incorrect" );

        Utils::Error( __FILE__,__FUNCTION__,__LINE__,strType,strValue );

        return false;
    }

    if( m_InitializationPaths.find( "Plugins" ) != m_InitializationPaths.end() )
    {

        if( !LoadPluginSettings() )

            return false;

    }

    else
    {
        std::string strType("Incorrect file");
        std::string strValue = strInitFile + std::string( " is incorrect" );
        Utils::Error( __FILE__,__FUNCTION__,__LINE__,strType,strValue );


        return false;
    }

    InitFile.close();

    m_Root = OGRE_NEW Ogre::Root( "","","" );

    if( !m_Root )
    {
        std::string strType("Ogre create fail");
        std::string strValue("Cannot create Ogre root object");

        Utils::Error( __FILE__,__FUNCTION__,__LINE__,strType,strValue );


        return false;
    }

    if( !LoadPlugins() )
    {
        return false;
    }

    if( !InitRenderSystem() )
    {
        return false;
    }

    if( !InitVideoSettings() )
    {
        return false;
    }

    if( ! InitialiseRoot() )
    {
        return false;
    }

    if( ! InitResources() )
    {
        return false;
    }

    return true;

}

bool Core::LoadVideoSettings()
{
    PathInitializator::iterator itPath = m_InitializationPaths.find( "VSettings" );
    std::ifstream   VSettingsFile;

    VSettingsFile.open( ( *itPath ).second.c_str() ,std::ifstream::in );

    if( !VSettingsFile.is_open() )
    {
        std::string strType("Missing file");
        std::string strValue = (*itPath).second + std::string( " missing \n"  );

        Utils::Error( __FILE__,__FUNCTION__,__LINE__,strType,strValue );

        return false;
    }

    std::vector<char>   vchTempBuffer( (std::istreambuf_iterator<char>( VSettingsFile )), std::istreambuf_iterator<char>() );
    rapidxml::xml_document<>    PathsFromFile;

    vchTempBuffer.push_back('\0');
    PathsFromFile.parse<0>( &vchTempBuffer[0] );

    rapidxml::xml_node<>    *xmlRoot = PathsFromFile.first_node();

    for (rapidxml::xml_node<> *child = xmlRoot->first_node(); child; child = child->next_sibling())
    {
        std::string strName;
        std::string strValue;

        strName = child->first_attribute()->value();
        strValue = child->value();

        if( strValue == "NA")
        {

        }

        else
        {
            m_VSettings.insert( std::pair< std::string,std::string >( strName,strValue ) );
        }

        strName.clear();
        strValue.clear();

    }

    VSettingsFile.close();

    return true;

}

bool Core::LoadPluginSettings()
{
    PathInitializator::iterator itPath = m_InitializationPaths.find( "Plugins" );

    std::ifstream   PluginsFile;

    PluginsFile.open( ( *itPath ).second.c_str() ,std::ifstream::in );

    if( !PluginsFile.is_open() )
    {
        std::string strType("Missing file");
        std::string strValue = (*itPath).second + std::string( " missing \n"  );

        Utils::Error( __FILE__,__FUNCTION__,__LINE__,strType,strValue );

        return false;
    }

    std::vector<char>   vchTempBuffer( ( std::istreambuf_iterator<char>( PluginsFile ) ), std::istreambuf_iterator<char>( ) );
    rapidxml::xml_document<>    PluginsFromFile;

    vchTempBuffer.push_back('\0');
    PluginsFromFile.parse<0>(&vchTempBuffer[0]);

    rapidxml::xml_node<>  *xmlRoot = PluginsFromFile.first_node();
    std::string strPluginsPath = xmlRoot->first_attribute()->value();

    for (rapidxml::xml_node<> *child = xmlRoot->first_node(); child; child = child->next_sibling())
    {
        std::string strName;
        std::string strValue;

        strName = child->name();
        strValue = child->value();

        if( strValue == "NA")
        {

        }

        else
        {
            m_Plugins.Insert( strName ,strPluginsPath + strValue );
        }

        strName.clear();
        strValue.clear();

    }

    PluginsFile.close();

    return true;

}

bool Core::LoadPlugins()
{
    for( Plugins::iterator it = m_Plugins.Begin();it != m_Plugins.End();it++ )
    {
        try
        {
            m_Root->loadPlugin( ( *it ).second );
        }

        catch( Ogre::Exception& e )
        {
            std::string strType("Cannot load plugin");
            std::string strValue =e.getFullDescription();

            Utils::Error( __FILE__,__FUNCTION__,__LINE__,strType,strValue );

            return false;

        }
    }

    return true;

}

bool Core::InitRenderSystem()
{

    m_Renderer =  m_Root->getAvailableRenderers().front();

    if( !m_Renderer )
    {
        std::string strType("Render system is unavailable");
        std::string strValue =m_Plugins["RenderSystem"];

        Utils::Error( __FILE__,__FUNCTION__,__LINE__,strType,strValue );

        return false;

    }

    return true;

}

bool Core::InitVideoSettings()
{
    try
    {
        for( VideoSettings::iterator it = m_VSettings.begin();it != m_VSettings.end();it++ )
        {
            m_Renderer->setConfigOption( it->first,it->second);
        }
    }

    catch( Ogre::Exception& e )
    {
        std::string strType("Cannot set video settings");
        std::string strValue =e.getFullDescription();

        Utils::Error( __FILE__,__FUNCTION__,__LINE__,strType,strValue );

        return false;
    }

    return true;
}

bool Core::InitialiseRoot()
{
    try
    {
        m_Root->setRenderSystem( m_Renderer );
    }

    catch( Ogre::Exception& e )
    {
        std::string strType("Cannot set Render system");
        std::string strValue =e.getFullDescription();

        Utils::Error( __FILE__,__FUNCTION__,__LINE__,strType,strValue );

        return false;
    }

    try
    {
        m_Root->initialise(false);
    }

    catch( Ogre::Exception& e )
    {
        std::string strType("Cannot initialise Root object");
        std::string strValue =e.getFullDescription();

        Utils::Error( __FILE__,__FUNCTION__,__LINE__,strType,strValue );

        return false;
    }

    return true;
}

bool Core::InitResources()
{
//    if( m_InitializationPaths.find( "Resources" ) == m_InitializationPaths.end() )
//    {
//            std::string strType("Resources section is not pressent");
//            std::string strValue("Resources section is not pressent in InitFile");

//            Utils::Error( __FILE__,__FUNCTION__,__LINE__,strType,strValue );

//            return false;
//    }

    Ogre::ConfigFile cf;
    try
    {
        cf.load(m_InitializationPaths.find( "Resources" )->second);
    }

    catch( Ogre::Exception& e )
    {
        std::string strType("Missing file");
        std::string strValue =e.getFullDescription();

        Utils::Error( __FILE__,__FUNCTION__,__LINE__,strType,strValue );

        return false;
    }

    // Go through all sections & settings in the file
    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
    Ogre::String secName, typeName, archName;

    while (seci.hasMoreElements())
    {
        secName = seci.peekNextKey();

        Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;

        for (i = settings->begin(); i != settings->end(); ++i)
        {
            typeName = i->first;
            archName = i->second;

                Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
        }

    }

    return true;
}

//bool Core::InitGame( std::string strGameFileName )
//{





//    return true;
//}

